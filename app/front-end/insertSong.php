<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="../assets/bootstrap-5.1.3-dist/css/bootstrap.min.css">
    <title>Document</title>
</head>
    <body>



        <div class="container">
            <div class="row">
                <div class="col-md-4 offset-md-4">
                    <div class="login-form bg-light mt-4 p-4">
                        <form action="../back-end/php/InsertSong.php" method="post" class="row g-3">
                            <h4>Add songs</h4>
                            <div class="col-12">
                                <label>Title</label>
                                <input type="text" name="title" class="form-control" placeholder="Title">
                            </div>
                            <div class="col-12">
                                <label>Album</label>
                                <input type="text" name="album" class="form-control" placeholder="Album" required>
                            </div>
                            <div class="col-12">
                                <label>Password</label>
                                <input type="password" name="password" class="form-control" placeholder="Password" required>
                            </div>
                            <div class="col-12">
                                <button type="submit" class="btn btn-dark float-end">Login</button>
                            </div>
                        </form>
                        <hr class="mt-4">
                    </div>
                </div>
            </div>
        </div>

    </body>
</html>
