<?php


class Constants {
    public static $emailUsed = "This email is already used";
    public static $passwordNotMatch = "Password don't match";
    public static $emailInvalid = "Your email is invalid";
    public static $usernameUsed = "Username already used";
}

