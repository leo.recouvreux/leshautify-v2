<?php 


class Dbh {
    private $db_name = 'leshautify';
    private $db_user = 'admin';
    private $db_host = 'localhost';
    private $db_password = 'password';

    protected function connect () {
        try {

            //se connecter à mysql
            $pdo = new PDO ("mysql:host=$this->db_host; dbname=$this->db_name", $this->db_user, $this->db_password );
            //COMPREND PAS A QUOI ÇA SERT REGARDER
            //g compri (merci Nicolas)
            //transforme l'erreur en exception et le transforme en try catch
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            $pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        } catch (Exception $e){
            die('Erreur: '.$e->getMessage());
        }
        return $pdo;
    }


}